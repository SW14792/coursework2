import numpy as np
import pylab as pl
from math import pi
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve as solve
import matplotlib.animation as animation
import matplotlib.pyplot as plt

"""Solver for 1D Heat equation with homogenous dirichlet boundary conditions
"""

#Initial Condition
def u_I(x, L):
    #initial condition: sin(pi*x) for now
    y = np.sin(pi*x/L)
    return y

#Exact Solution to homogenous boudary condition
def u_ex(x, k, L, T):
    y = np.exp(-k*(pi**2/L**2)*T)*np.sin(pi*x/L)
    return y

#Forward Euler Matrix
def F_Matrix(l, n):
    #Sets up forward Euler matrix with lambda = l of dimension n
    a = (1-2*l)*np.ones(n)
    b = l*np.ones(n-1)
    diagonals = [a, b, b]
    A = diags(diagonals, [0, -1, 1]).toarray()
    return A

#Solver - Forward Euler
def Heat_f(k, L, T, u_I, mx, mt):
    """
    Forward Euler Solver for heat equation with homegenous dirichlet boundary conditions on domain
    L, for time period T, with initial condition f and spatial and time discretization mx and mt
    """
    dx = L/(mx) #Grid spacing for x
    dt = (T/mt) #Grid spacing for t
    lm = k*dt/(dx**2) #resulting lambda

    A = F_Matrix(lm, mx+1) #setting up matrix

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = np.dot(A, u)

    return u

#Backward Euler Matrix
def B_Matrix(l, n):
    #Sets up backward euler matrix with lambda = l of dimension n
    a = (1+2*l)*np.ones(n)
    b = -l*np.ones(n-1)
    diagonals = [a, b, b]
    B = diags(diagonals, [0, -1, 1]).toarray()
    return B

#Solver - Backward Euler
def Heat_b(k, L, T, u_I, mx, mt):
    """Backward Euler Solver for heat equation with homogenous dirichlet boundary conditions on domain
    L, for time period T, wit initial condition u_I and spatial andf time disretization mx and mt
    """
    dx = L/mx #spatial grid spacing
    dt = (T/mt) #time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    A = B_Matrix(lm, mx+1) #Setting up Matrix

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = solve(A, u)

    return u

#Crank-Nicholson Matrix
def CN_Matrix(l,n):
    #Sets up matrices for the Crank-Nicholson Scheme
    a1 = (1+l)*np.ones(n) #Diagonals for A matrix
    a2 = -l/2*np.ones(n-1)
    b1 = (1-l)*np.ones(n) #Diagonals for B matrix
    b2 = l/2*np.ones(n-1)

    adiagonals = [a1, a2, a2]
    bdiagonals = [b1, b2, b2]

    A = diags(adiagonals, [0, 1, -1]).toarray()
    B = diags(bdiagonals, [0, 1, -1]).toarray()

    return A, B

#Solver - Crank-Nicholson
def Heat_cn(k,L, T, u_I, mx, mt):
    """Crank Nicholson scheme solver for heat equation with homogenous dirichlet boundary conditions on Domain
    L for time period T, initial condition u_I, with spatial and time discretisations mx, and mt
    """

    dx = L/mx #spatial grid spacing
    dt = (T/mt) #time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    A, B = CN_Matrix(lm, mx +1) #Matrices set up

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = solve(A, np.dot(B, u))

    return u

#Non-Homegenous boundary condition function
def B1(x):
    """Using simple case of f(t) = t and g(t) = t^2"""
    f = x
    return f

def B2(x):
    """Using simple case of f(t) = t and g(t) = t^2"""
    g = x**2
    return g

#Solver - Forward Euler with Non-Homogenous Dirichlet Boundary Condition
def Heat_f_NH(k, L, T, u_I, BC1, BC2, mx, mt):
    """
    Forward Euler solver for heat equation with non-homogenous dirichlet boundary conditions
    """
    dx = L/mx #Spatial Grid Spacing
    dt = T/mt #Time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    u = np.linspace(0, L, mx+1)

    A = F_Matrix(lm, mx+1) #Setting up Matrix

    v = np.transpose(np.zeros(mx+1)) #Setting up boundary condition vector
    v[0] = BC1(u[0])
    v[mx] = BC2(u[mx])

    for i in range (0, mt):
        u = np.dot(A, u) + lm*v
        v[0] = BC1(u[0])
        v[mx]= BC2(u[mx])

    return u

def Heat_Source(x):
    #simple linear heat source function
    f = -x**2

    return f

def Heat_f_source(k, L, T, u_I, F, mx, mt):
    """
    Forward Euler Solver fort heat equation with a heat source F
    """
    dx = L/mx #Spatial Grid spacing
    dt = T/mt #Time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    A = F_Matrix(lm, mx+1) #setting up matrix

    u = np.linspace(0, L, mx+1) #Setting up domain and applying initial condition
    u = u_I(u, L)

    S = np.transpose(F(u)) #Heat source vector

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = np.dot(A, u) + mt*S

    return u


"""
Error Analysis

#Spatial Grid Mesh vs Error for Forward Euler
e = np.zeros(10)
m = np.zeros(10)
for i in range (0, 10):
    mx = 11 - i
    m[i] = mx
    x = np.linspace(0, 1, mx+1)
    u = Heat_f(1.0, 1.0, 0.5, u_I, mx, 1000)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum((u - v))/(mx+1))


m = np.log(m)
e = np.log(e)
O = (e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
plt.gca().invert_xaxis()
pl.xlabel('Log(Mesh Size mx)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = 2.41',(1.5,-6))
pl.title('Error vs Varying mx - Forward')
pl.show

#Time Grid Mesh vs Error for Forward Euler
e = np.zeros(10)
m = np.zeros(10)
for i in range (0, 10):
    mt = 1000 + 200*i
    m[i] = mt
    x = np.linspace(0, 1, 11)
    u = Heat_f(1.0, 1.0, 0.5, u_I, 10, mt)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum(u - v))/(11)


e = np.log(e)
m = np.log(m)
O = (e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
plt.gca().invert_xaxis()
pl.xlabel('Log(Mesh Size mt)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = -0.19',(7.4,-8.8))
pl.title('Error vs Varying mt - Forward')
pl.show()


#Spatial Grid Mesh vs Error for Backward Euler
e = np.zeros(10)
m = np.zeros(10)
for i in range (0,10):
    mx = 10 + i
    m[i] = mx
    x = np.linspace(0, 1, mx+1)
    u = Heat_b(1.0, 1.0, 0.5, u_I, mx, 1000)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum(u - v))/(mx+1)

e = np.log(e)
m = np.log(m)
O = (e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
pl.xlabel('Log(Mesh Size mx)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = 0.23',(2.7,-7.10))
pl.title('Error vs Varying mx - Backward')
pl.show()

#Time Grid Mesh vs Error for Backward Euler
e = np.zeros(10)
m = np.zeros(10)
for i in range (0, 10):
    mt = 1000 + 200*i
    m[i] = mt
    x = np.linspace(0, 1, 11)
    u = Heat_b(1.0, 1.0, 0.5, u_I, 10, mt)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum(u - v))/(11)


e = np.log(e)
m = np.log(m)
O = -(e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
plt.gca().invert_xaxis()
pl.xlabel('Log(Mesh Size mt)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = 0.72',(7.4,-7.6))
pl.title('Error vs Varying mt - Backward')
pl.show()

#Spatial Grid Mesh vs Error for Crank-Nicholson
e = np.zeros(10)
m = np.zeros(10)
for i in range (0,10):
    mx = 10 + i
    m[i] = mx
    x = np.linspace(0, 1, mx+1)
    u = Heat_cn(1.0, 1.0, 0.5, u_I, mx, 1000)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum(u - v))/(mx+1)

e = np.log(e)
m = np.log(m)
O = (e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
pl.xlabel('Log(Mesh Size mx)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = 0.16',(2.7, -7.74))
pl.title('Error vs Varying mx - CN')
pl.show()

#Time Grid Mesh vs Error for Crank-Nicholson
e = np.zeros(10)
m = np.zeros(10)
for i in range (0, 10):
    mt = 1000 + 200*i
    m[i] = mt
    x = np.linspace(0, 1, 11)
    u = Heat_cn(1.0, 1.0, 0.5, u_I, 10, mt)
    v = u_ex(x, 1.0, 1.0, 0.5)
    e[i] = (np.sum(u - v))/(11)


e = np.log(e)
m = np.log(m)
O = -(e[1]-e[0])/(m[1]-m[0])
pl.plot(m, e, 'go')
plt.gca().invert_xaxis()
pl.xlabel('Log(Mesh Size mt)')
pl.ylabel('Log(Average Error)')
pl.annotate('Gradient = 0.55',(7.2, -8.2))
pl.title('Error vs Varying mt - CN')
pl.show()
"""



def animate(i):
    line.set_ydata(Heat_f_NH(1.0, 1.0, 0 + i*0.001, u_I, B1, B2, 10, 100))
    return line,

def init():
    line.set_ydata(np.ma.array(x, mask=True))
    return line,




fig, ax = plt.subplots()
x = np.linspace(0, 1, 11)
line,  = ax.plot(x, u_I(x, 1))

ani = animation.FuncAnimation(fig, animate, np.arange(1, 200), init_func=init, interval=25, blit=True)
plt.show()
"""

x = np.linspace(0,1,11)
u_cn = Heat_cn(1.0, 1.0, 0.5, u_I, 10, 1000)
u_f = Heat_f(1.0, 1.0, 0.5, u_I, 10, 1000)
u_b = Heat_b(1.0, 1.0, 0.5, u_I, 10, 1000)

pl.plot(x, u_cn, x, u_f, x, u_b, x, u_ex(x, 1, 1, 0.5), 'go') #GO OVER THIS BECAUSE U_ex IS NOT ALIGNING!!!
pl.show()
"""
