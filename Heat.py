import numpy as np
import pylab as pl
from math import pi
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve as solve

"""Solver for 1D Heat equation with homogenous dirichlet boundary conditions
"""

#Initial Condition
def u_I(x, L):
    #initial condition: sin(pi*x) for now
    y = np.cos(pi*x/L)
    return y

#Exact Solution to homogenous boudary condition
def u_ex(x, k, L, T):
    y = np.exp(-k*(pi**2/L**2)*T)*np.sin(pi*x/L)
    return y

#Forward Euler Matrix
def F_Matrix(l, n):
    #Sets up forward Euler matrix with lambda = l of dimension n
    a = (1-2*l)*np.ones(n)
    b = l*np.ones(n-1)
    diagonals = [a, b, b]
    A = diags(diagonals, [0, -1, 1]).toarray()
    return A

#Solver - Forward Euler
def Heat_f(k, L, T, u_I, mx, mt):
    """
    Forward Euler Solver for heat equation with homegenous dirichlet boundary conditions on domain
    L, for time period T, with initial condition f and spatial and time discretization mx and mt
    """
    dx = L/(mx) #Grid spacing for x
    dt = (T/mt) #Grid spacing for t
    lm = k*dt/(dx**2) #resulting lambda

    A = F_Matrix(lm, mx+1) #setting up matrix

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = np.dot(A, u)

    return u

#Backward Euler Matrix
def B_Matrix(l, n):
    #Sets up backward euler matrix with lambda = l of dimension n
    a = (1+2*l)*np.ones(n)
    b = -l*np.ones(n-1)
    diagonals = [a, b, b]
    B = diags(diagonals, [0, -1, 1]).toarray()
    return B

#Solver - Backward Euler
def Heat_b(k, L, T, u_I, mx, mt):
    """Backward Euler Solver for heat equation with homogenous dirichlet boundary conditions on domain
    L, for time period T, wit initial condition u_I and spatial andf time disretization mx and mt
    """
    dx = L/mx #spatial grid spacing
    dt = (T/mt) #time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    A = B_Matrix(lm, mx+1) #Setting up Matrix

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = solve(A, u)

    return u

#Crank-Nicholson Matrix
def CN_Matrix(l,n):
    #Sets up matrices for the Crank-Nicholson Scheme
    a1 = (1+l)*np.ones(n) #Diagonals for A matrix
    a2 = -l/2*np.ones(n-1)
    b1 = (1-l)*np.ones(n) #Diagonals for B matrix
    b2 = l/2*np.ones(n-1)

    adiagonals = [a1, a2, a2]
    bdiagonals = [b1, b2, b2]

    A = diags(adiagonals, [0, 1, -1]).toarray()
    B = diags(bdiagonals, [0, 1, -1]).toarray()

    return A, B

#Solver - Crank-Nicholson
def Heat_cn(k,L, T, u_I, mx, mt):
    """Crank Nicholson scheme solver for heat equation with homogenous dirichlet boundary conditions on Domain
    L for time period T, initial condition u_I, with spatial and time discretisations mx, and mt
    """

    dx = L/mx #spatial grid spacing
    dt = (T/mt) #time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    A, B = CN_Matrix(lm, mx +1) #Matrices set up

    u = np.linspace(0, L, mx + 1) #domain with initial condition applied
    u = np.transpose(u_I(u, L))

    for i in range (0, mt):
        u[0] = 0; u[mx] = 0
        u = solve(A, np.dot(B, u))

    return u

#Non-Homegenous boundary condition function
def B1(x):
    """Using simple case of f(t) = t and g(t) = t^2"""
    f = x
    return f

def B2(x):
    """Using simple case of f(t) = t and g(t) = t^2"""
    g = x**2
    return g

#Solver - Forward Euler with Non-Homogenous Dirichlet Boundary Condition
def Heat_f_NH(k, L, T, u_I, BC1, BC2, mx, mt):
    """
    Forward Euler solver for heat equation with non-homogenous dirichlet boundary conditions
    """
    dx = L/mx #Spatial Grid Spacing
    dt = T/mt #Time grid spacing
    lm = k*dt/(dx**2) #resulting lambda

    u = np.linspace(0, L, mx+1)

    A = F_Matrix(lm, mx+1) #Setting up Matrix

    v = np.transpose(np.zeros(mx+1)) #Setting up boundary condition vector
    v[0] = BC1(u[0])
    v[mx] = BC2(u[mx])

    for i in range (0, mt):
        u = np.dot(A, u) + lm*v
        v[0] = BC1(u[0])
        v[mx]= BC2(u[mx])

    return u

def visualize(u, t, I, L, savefig=False, skip_frames=1):
    """
    Animates u against t for a long time period
    """
    import scitools.std as std
    from scitools.MovingPlotWindow import MovingPlotWindow

    #Remove all old plot files (if any)
    import glob, os
    for filename in glob.glob('tmp_*.png'):
        os.remove(filename)
    P = pi/L
    umin = 1.2*u.min(); umax = -umin
    dt = t[1] - t[0]
    plot_manager = MovingPlotWindow(
        window_width = 8*P,
        dt = dt,
        yaxis = [umin, umax],
        mode = 'continuous drawing')
    frame_counter = 0
    for n in range(1, len(u)):
        if plot_manager.plot(n):
            s = plot_manager.first_index_in_plot
            st.plot(t[s:n+1], u[s:n+1], 'r-1',
                    t[s:n+1], I*cos(L*t)[s:n+1], 'b-1',
                    title='t=%6.3f' % t[n],
                    axis=plot_manager.axis(),
                    show=not savefig)
            if savefig and n % skip_frames == 0:
                filename = 'tmp_%04d.png' % frame_counter
                st.savefig(filename)
                print 'making plot file', filename, 'at t=%g' % t[n]
                frame_counter +=1
        plot_manager.update(n)

t = np.linspace(0, 0.5, 1001)
visualize(Heat_b, t, u_I, 1, savefig=False, skip_frames=1)

"""

x = np.linspace(0,1,11)
u_cn = Heat_cn(1.0, 1.0, 0.5, u_I, 10, 1000)
u_f = Heat_f(1.0, 1.0, 0.5, u_I, 10, 1000)
u_b = Heat_b(1.0, 1.0, 0.5, u_I, 10, 1000)

pl.plot(x, u_cn, x, u_f, x, u_b, x, u_ex(x, 1, 1, 0.5), 'go') #GO OVER THIS BECAUSE U_ex IS NOT ALIGNING!!!
pl.show()
"""
