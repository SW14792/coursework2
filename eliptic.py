import numpy as np
import pylab as pl
from math import pi
import matplotlib.pyplot as plt

#Boundary Conditions
def Bound(x, y, Lx, Ly):
    """Returns 4 boundary condition functions in the order
    y=0, y=Ly, x=0, x=Lx """

    def fB(x):
        u = np.zeros(x.size)
        return u
    def fT(x):
        u = np.sin(pi*x/Lx)
        return u
    def fL(y):
        u = np.zeros(y.size)
        return u
    def fR(y):
        u = np.zeros(y.size)
        return u
    return fB, fT, fL, fR

#Exact Solution Function
def u_exact(x, y):
    y = np.sin(pi*x/Lx)*np.sinh(pi*y/Lx)/np.sinh(pi*Ly/Lx)
    return y

#Laplaces Equation solver function using Successive Over Relaxation Method
def Laplace_SOR(Lx, Ly, mx, my, maxerr, maxcount, Bound, w):
    """Lx, Ly - Length of x and y domain; mx, my - x and y mesh number; maxerr - target error
    maxcount - maximum number of iterations; Bound- Boundary condition function, w - 'relaxation coeficient'"""

    #Setting up x & y domains
    x = np.linspace(0, Lx, mx+1)
    y = np.linspace(0, Ly, my+1)

    #Calculating gridspacing and resulting mesh number
    dx = x[1] - x[0]
    dy = y[1] - y[0]
    lm = (dx/dy)**2

    #Solution variables
    u_o = np.zeros((x.size, y.size))
    u_n = np.zeros((x.size, y.size))

    #Import boundary condition
    fB, fT, fL, fR = Bound(x, y, Lx, Ly)

    #Apply boundary conditions to solution variables
    u_o[1:-1,0] = fB(x[1:-1])
    u_o[1:-1,-1]= fT(x[1:-1])
    u_o[0, 1:-1]= fL(y[1:-1])
    u_o[-1, 1:-1]= fR(y[1:-1])
    u_n[:] = u_o[:]

    #Set up error and counters
    err = maxerr+1
    count = 1

    #Now solve!
    while err>maxerr and count<maxcount:
        for j in range(1,my):
            for i in range (1, mx):
                u_n[i,j] = u_o[i,j] + (w/(2*(1+lm**2)))*(u_n[i-1, j]+u_o[i+1,j]- \
                2*(1+lm**2)*u_o[i,j]+lm**2*u_n[i, j-1] + lm**2*u_o[i, j+1])
        err = np.max(np.abs(u_n-u_o))
        u_o[:] = u_n[:]
        count = count + 1
    return u_n, err



# set dimensions of spatial domain
Lx=2.0
Ly=1.0

# set numerical parameters
mx = 40              # number of gridpoints in x
my = 20              # number of gridpoints in y
maxerr = 1e-4        # target error
maxcount = 1000      # maximum number of iteration steps
w = 1.2

# set up the numerical environment variables
x = np.linspace(0, Lx, mx+1)     # mesh points in x
y = np.linspace(0, Ly, my+1)     # mesh points in y
dx = x[1] - x[0]             # gridspacing in x
dy = y[1] - y[0]             # gridspacing in y

#Create array of err for varying values of w
e = np.zeros(10)
for i in range (0, 10):
    u, er = Laplace_SOR(Lx, Ly, mx, my, maxerr, maxcount, Bound, 1 + 0.1*i)
    e[i] = er

print (e)
