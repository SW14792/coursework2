#Coursework
import numpy as np
import pylab as pl
from math import pi

k = 1
L = 1.0
T = 0.5
mx = 10
mt = 1000
x = np.linspace(0, L, mx+1)     # mesh points in space
t = np.linspace(0, T, mt+1)     # mesh points in time
deltax = L/mx
deltat = T/mt
#import ipdb; ipdb.set_trace()
lmbda = k*deltat/(deltax**2)

""" Defining initial conditions"""
def init(x):
    return np.sin(pi*x)

""" Define actual solution"""
def sol(x):
    y = np.exp(-(pi**2)*0.5)*np.sin(pi*x)
    return y


"""Building Matrix multiplier"""
u = np.linspace(0, L, mx + 1)
u = np.transpose(init(u))
A = np.zeros((mx + 1, mx +1))
np.fill_diagonal(A, 1 - 2*lmbda)
B = lmbda*np.ones((mx + 1, mx+ 1))
A = A + np.diag(np.diag(B, -1), -1) + np.diag(np.diag(B, 1), 1)

def iterate(n, u):
    for i in range (0,n):
        u = np.dot(A, u)
    return u

a = iterate(0, u)
b = iterate(50, u)
c = iterate(100, u)
d = iterate(500, u)
e = iterate(1001, u)

pl.plot(x, a, x, b, x, c, x, d, x, e)
pl.plot(x, sol(x))
pl.show()
