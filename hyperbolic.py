import numpy as np
import pylab as pl
from math import pi
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve as solve
import matplotlib.animation as animation
import matplotlib.pyplot as plt

def init1(x, L):
    """Initial conditions - displacement and velocity"""
    uI = np.sin(2*pi*x/L)
    vI = np.zeros(x.size)

    return uI, vI

def u_exact(x,t):
    """the exact solution"""
    y = np.cos(pi*c*t/L)*np.sin(pi*x/L)
    return y

def F_Matrix(c, n):
    """Sets up matrix with courant number = c and dimension n"""
    a = (2-2*c**2)*np.ones(n) #Leading Diagonal
    b = (c**2)*np.ones(n-1) #Off-Diaonals
    diagonals = [a, b, b]
    A = diags(diagonals, [0, -1, 1]).toarray()

    return A

def Wave_E(k, L, T, init, mx, mt):
    """
    Explicit solver for 1D wave equation
    """
    dx = L/mx   #Spatial Grid spacing
    dt = T/mt #Time grid spacing
    c = np.sqrt(k)*dt/dx #Resulting courant number

    A = F_Matrix(c, mx+1) #Setting up explicit matrix

    u = np.linspace(0, L, mx+1)

    uI, vI = init(u, L) #Applying initial conditions
    u = np.transpose(uI)

    v = u - 2*dt*np.transpose(vI)


    for i in range (0, mt):
        t = u
        u = np.dot(A,u) - v
        v = t
        u[0]=0; u[mx]=0
    return u

def I_Matrix(c, n):
    """Outputs Backward and Forward implicit matrices - the matrices are the negative of each other"""
    a = (1+c**2)*np.ones(n) #leading diagonal for both matricies
    b = -(c**2/2)*np.ones(n-1) #off-diagonals
    diagonals = [a, b, b]

    A = diags(diagonals, [0, 1, -1]).toarray()
    B = -1*A

    return A, B

def Wave_I(k, L, T, init, mx, mt):
    """Implicit solver for 1D wave equatin"""
    dx = L/mx   #Spatial Grid spacing
    dt = T/mt #Time grid spacing
    c = np.sqrt(k)*dt/dx #Resulting courant number

    A, B = I_Matrix(c, mx+1)

    u = np.linspace(0, L, mx+1)

    uI, vI = init(u, L) #Applying initial conditions
    u = np.transpose(uI)

    v = u - 2*dt*np.transpose(vI)

    for i in range (0, mt):
        t = u
        u = solve(A, 2*u + np.dot(B, v))
        v = t
        u[0] = uI[0]; u[mx] = uI[mx]

    return u

def B1(x):
    f = 0
    return f

def B2(x):
    g = np.sin(pi*x)/4
    return g

def Wave_E_NH(k, L, T, init, B1, B2, mx, mt):
    """Wave equation with non-homogenous boundary conditions"""
    dx = L/mx   #Spatial Grid spacing
    dt = T/mt #Time grid spacing
    c = np.sqrt(k)*dt/dx #Resulting courant number

    A = F_Matrix(c, mx+1) #Setting up explicit matrix

    u = np.linspace(0, L, mx+1)

    uI, vI = init(u, L) #Applying initial conditions

    u = np.transpose(uI)
    v = u - 2*dt*np.transpose(vI)

    l = np.transpose(np.zeros(mx+1))
    l[0] = B1(u[0]); l[mx] = B2(u[mx])

    for i in range (0, mt):
        t = u
        u = np.dot(A,u) - v + c*l
        v = t
        u[0] = 0; l[mx] = B2(u[mx])

    return u


def h(i):
    h = i
    return h

def H_matrix(hp, hm, n):
    """Returns the evolution matrix for the tidal problem from the hyperbolic work sheet"""
    a = hm #diagonal -1
    b = -(h(i-1/2)+h(i+1/2))*np.ones(n) #Leading diagonal
    c = h(i+1/2)*np.ones(n-1) # diagonal 1
    diagonals = [a, b, c]

    H = diags(diagonals, [-1, 0, 1]).toarray()

    return H



def Tidal(L, T, init, h, mx, mt):

    """Tidal problem from hyperbolic work sheet"""
    dx = L/mx   #Spatial Grid spacing
    dt = T/mt #Time grid spacing
    c = (dt**2)/(dx**2)

    H = H_matrix(h, mx+1) #Setting up tranformation matrix

    uI, vI = init(u, L) #Applying initial Conditions
    u = np.transpose(uI)

    v = u - 2*dt*np.transpose(vI)

    for i in range (0, mt):
        t = u
        u = c*np.dot(Q, u) + 2*u - v
        v = t

    return u


c = 1.0
L = 1.0
T = 10
mx = 30
mt = 50
x = np.linspace(0, L, mx+1)

def animate(i):
    line.set_ydata(Tidal(L, 0.01*i, init1, h, mx, mt))
    return line,

def init():
    line.set_ydata(np.ma.array(x, mask=True))
    return line,

uI, vI = init1(x, L)


fig, ax = plt.subplots()
x = np.linspace(0, 1, mx+1)
line,  = ax.plot(x, uI)

ani = animation.FuncAnimation(fig, animate, np.arange(1, 200), init_func=init, interval=25, blit=True)
plt.show()

"""
u_e = u_exact(x, T)
u = Wave_E(c, L, T, init, mx, mt)

pl.plot(x, u_e, 'go', label='exact')
pl.plot(x, u, 'r-', label = 'numeric')
pl.show()
"""
